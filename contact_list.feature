Feature: Contact List
    
   Background:
     Given we visit the DigitalEd Assignment homepage
  
   Scenario: Title is displayed
     Then the page's title says "DigitalEd QA Assignment: Contact List App"

   Scenario: Create a new contact
     When I create a new contact with the name "George Brown", phone number "(333) 333-3333" and email "gbrown@example.com"
     Then the contact with the name "George Brown", phone number "(333) 333-3333" and email "gbrown@example.com" is displayed in row 1

   Scenario: Buttons are functioning
     When I create a new contact with the name "George Brown", phone number "(333) 333-3333" and email "gbrown@example.com"
     Then I confirm buttons are functioning

   Scenario: Create a new contact with a very long name
     When I create a new contact with the name "uvuvwevwevwe onyetenyevwe ugwemuhwem osas uvuvwevwevwe onyetenyevwe ugwemuhwem osas", phone number "(888) 888-8888" and email "uosas@example.com"
     Then the contact with the name "uvuvwevwevwe onyetenyevwe ugwemuhwem osas uvuvwevwevwe onyetenyevwe ugwemuhwem osas", phone number "(888) 888-8888" and email "uosas@example.com" is displayed in row 1

   Scenario: Create a new contact with non latin characters
     When I create a new contact with the name "尾田 栄一郎", phone number "(777) 777-7777" and email "eoda@example.com"
     Then the contact with the name "尾田 栄一郎", phone number "(777) 777-7777" and email "eoda@example.com" is displayed in row 1

   Scenario: Create a new contact with symbols
     When I create a new contact with the name "Hannah \"Max\" O'Keefe? & Family%", phone number "(226)" and email "?@example.com"
     Then the contact with the name "Hannah \"Max\" O'Keefe? & Family%", phone number "(226)" and email "?@example.com" is displayed in row 1

   Scenario: Create an empty contact
     When I create an empty contact with no information
     Then there should be 0 contacts

   Scenario: Create 3 new contacts
     When I create a new contact with the name "Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com"
     Then the contact with the name "Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com" is displayed in row 1
     When I create a new contact with the name "Jerry Mouse", phone number "(555) 555-5555" and email "jmouse@example.com"
     Then the contact with the name "Jerry Mouse", phone number "(555) 555-5555" and email "jmouse@example.com" is displayed in row 2
     When I create a new contact with the name "Spike Dog", phone number "(666) 666-6666" and email "sdog@example.com"
     Then the contact with the name "Spike Dog", phone number "(666) 666-6666" and email "sdog@example.com" is displayed in row 3

   Scenario: Update contact email
     When I create a new contact with the name "Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com"
     When I update the contact email with the email "newtcat@example.com" in row 1
     Then the contact with the name "Tom Cat", phone number "(444) 444-4444" and email "newtcat@example.com" is displayed in row 1

   Scenario: Update contact phone number
     When I create a new contact with the name "Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com"
     When I update the contact phone number with the phone number "New (444) 444-4444" in row 1
     Then the contact with the name "Tom Cat", phone number "New (444) 444-4444" and email "tcat@example.com" is displayed in row 1

   Scenario: Update contact name
     When I create a new contact with the name "Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com"
     When I update the contact name with the name "New Tom Cat" in row 1
     Then the contact with the name "New Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com" is displayed in row 1

   Scenario: Update all contact information
     When I create a new contact with the name "Tom Cat", phone number "(444) 444-4444" and email "tcat@example.com"
     When I update the contact with the name "New Tom Cat", phone number "New (444) 444-4444" and email "newtcat@example.com" in row 1
     Then the contact with the name "New Tom Cat", phone number "New (444) 444-4444" and email "newtcat@example.com" is displayed in row 1

   Scenario: Delete contacts
     When I create a new contact with the name "Tom 1", phone number "(444) 444-4444" and email "tcat@example.com"
     And I create a new contact with the name "Tom 2", phone number "(444) 444-4444" and email "tcat@example.com"
     And I create a new contact with the name "Tom 3", phone number "(444) 444-4444" and email "tcat@example.com"
     And I create a new contact with the name "Tom 4", phone number "(444) 444-4444" and email "tcat@example.com"
     And I create a new contact with the name "Tom 5", phone number "(444) 444-4444" and email "tcat@example.com"
     And I delete the first contact
     Then there should be 4 contacts
     And the contact with the name "Tom 2", phone number "(444) 444-4444" and email "tcat@example.com" is displayed in row 1
     When I delete contact in row 3
     Then there should be 3 contacts
     And the contact with the name "Tom 5", phone number "(444) 444-4444" and email "tcat@example.com" is displayed in row 3
     When I delete the last contact
     Then there should be 2 contacts
     And the contact with the name "Tom 3", phone number "(444) 444-4444" and email "tcat@example.com" is displayed in row 2
     When I delete all contacts
     Then there should be 0 contacts
