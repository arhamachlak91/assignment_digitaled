import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

const visitHomepage = () => {
    cy.visit('contact_list.html');
};
Given('we visit the DigitalEd Assignment homepage', visitHomepage);

const assertTitle = (expectedTitle) => {
    cy.title().should('eq', expectedTitle);
};
Then('the page\'s title says {string}', assertTitle);

const createContact = (name, phoneNumber, email) => {
    setInputText(getCreateContactDiv(NAME_INDEX), name)
    setInputText(getCreateContactDiv(PHONE_NUMBER_INDEX), phoneNumber);
    setInputText(getCreateContactDiv(EMAIL_INDEX), email);
    getButton('add').click();
    /** OR
    setCreateContactText(NAME_INDEX, name);
    setCreateContactText(PHONE_NUMBER_INDEX, phoneNumber);
    setCreateContactText(EMAIL_INDEX, email);
    clickButton('add');
    */
};
When('I create a new contact with the name {string}, phone number {string} and email {string}', createContact);

const assertContact = (name, phoneNumber, email, rowIndex) => {
    getRow(rowIndex).within(($row) => {
        assertRowCellText(NAME_INDEX, name);
        assertRowCellText(PHONE_NUMBER_INDEX, phoneNumber);
        assertRowCellText(EMAIL_INDEX, email);
    });
    /** OR
    assertSavedContactText(rowIndex, NAME_INDEX, name);
    assertSavedContactText(rowIndex, PHONE_NUMBER_INDEX, phoneNumber);
    assertSavedContactText(rowIndex, EMAIL_INDEX, email);
    */
};
Then('the contact with the name {string}, phone number {string} and email {string} is displayed in row {int}', assertContact);

const NAME_INDEX = 0;
const PHONE_NUMBER_INDEX = 1;
const EMAIL_INDEX = 2;

// My Addition: //////////////////////////////////////////////////////////////

const createEmptyContact = () => {
    setInputEmptyText(getCreateContactDiv(NAME_INDEX))
    setInputEmptyText(getCreateContactDiv(PHONE_NUMBER_INDEX));
    setInputEmptyText(getCreateContactDiv(EMAIL_INDEX));
    getButton('add').click();
};
When('I create an empty contact with no information', createEmptyContact);

const verifyRowCount = (rowCount) => {
    isRowCount(rowCount + 1);
};
Then('there should be {int} contacts', verifyRowCount);

const updateContactName = (name, rowIndex) => {
    clickButton('edit')
    setEditContactText(rowIndex, NAME_INDEX, name);
    clickButton('update');
};
When('I update the contact name with the name {string} in row {int}', updateContactName);

const updateContactPhoneNumber = (phoneNumber, rowIndex) => {
    clickButton('edit')
    setEditContactText(rowIndex, PHONE_NUMBER_INDEX, phoneNumber);
    clickButton('update');
};
When('I update the contact phone number with the phone number {string} in row {int}', updateContactPhoneNumber);

const updateContactEmail = (email, rowIndex) => {
    clickButton('edit')
    setEditContactText(rowIndex, EMAIL_INDEX, email);
    clickButton('update');
};
When('I update the contact email with the email {string} in row {int}', updateContactEmail);

const updateContactFull = (name, phoneNumber, email, rowIndex) => {
    clickButton('edit')
    setEditContactText(rowIndex, NAME_INDEX, name);
    setEditContactText(rowIndex, PHONE_NUMBER_INDEX, phoneNumber);
    setEditContactText(rowIndex, EMAIL_INDEX, email);
    clickButton('update');
};
When('I update the contact with the name {string}, phone number {string} and email {string} in row {int}', updateContactFull);

const deleteFirstContact = () => {
     getButton('delete').first().click();
};
When('I delete the first contact', deleteFirstContact);

const deleteLastContact = () => {
     getButton('delete').last().click();
};
When('I delete the last contact', deleteLastContact);

const deleteAllContact = () => {
     getButton('delete').click({ multiple: true });
};
When('I delete all contacts', deleteAllContact);

const deleteIndexedContact = (rowIndex) => {
    getRow(rowIndex).within(($row) => {
        getButton('delete').click();
    });
};
When('I delete contact in row {int}', deleteIndexedContact);

const confirmButtonFunctionality = () => {
    getRow(1).within(($row) => {
        getButtonVisibility('edit');
        getButtonVisibility('delete');
        getButton('edit').click();
        getButtonVisibility('update')
        getButton('update').click();
        getButtonVisibility('edit');
        getButtonVisibility('delete');
        getButton('delete').click();
    });
    isRowCount(1)
};
Then('I confirm buttons are functioning', confirmButtonFunctionality);

// We can use these. /////////////////////////////////////////////////////////

const setCreateContactText = (textInputIndex, text) => {
    cy.get('#app > div > div').eq(textInputIndex).find('input').clear().type(text);
};

const setEditContactText = (rowIndex, cellIndex, text) => {
    cy.get('#app tbody > tr').eq(rowIndex).find('td').eq(cellIndex).find('input').clear().type(text);
};

const clickButton = (buttonName) => {
    cy.get('button[name="' + buttonName + '"]').click();
};

const assertSavedContactText = (rowIndex, cellIndex, text) => {
    cy.get('#app tbody > tr').eq(rowIndex).find('td').eq(cellIndex).should('contain', text).should('be.visible');
};

// Or we can use these. /////////////////////////////////////////////////////////

const getCreateContactDiv = (divIndex) => {
    return cy.get('#app > div > div').eq(divIndex);
};

const getRow = (rowIndex) => {
    return cy.get('#app tbody > tr').eq(rowIndex);
};

const getCell = (cellIndex) => {
    return cy.get('td').eq(cellIndex);
}

const getButton = (buttonName) => {
    return cy.get('button[name="' + buttonName + '"]');
}

const setInputText = (element, text) => {
    element.find('input').clear().type(text);
}

const assertRowCellText = (cellIndex, text) => {
    cy.get('td').eq(cellIndex).should('contain', text).should('be.visible');
};

// My Addition: //////////////////////////////////////////////////////////////

const setInputEmptyText = (element) => {
    element.find('input').clear();
};

const isRowCount = (rCount) => {
    return cy.get('#app tbody > tr').should('have.length', rCount);
};

const getButtonVisibility = (buttonName) => {
    cy.get('button[name="' + buttonName + '"]').should('be.visible');
}