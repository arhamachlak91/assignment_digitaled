***********************************
*********** Test Cases ************

1. Check the app's title
    Step 1: Open the app and check its title
    Expected outcome: Title is "DigitalEd QA Assignment: Contact List App"

2. Create a contact
    Step 1: Open the app
    Step 2: Enter contact information: name, phone number, and email
    Step 3: Click on the "add" button
    Expected outcome: The entered name, phone number, and email are displayed in the first row

3. Button functionality
    Step 1: Open the app and create one contact
    Expected outcome: In the first row, we can find the "edit" and "delete" buttons
    Step 2: Click on the "edit" button
    Expected outcome: In the first row, now we can find the "update" button
    Step 3: Click on the "update" button
    Expected outcome: In the first row, again we can find the "edit" and "delete" buttons
    Step 4: Click on the "delete" button
    Expected outcome: The first contact is removed, as well as the "edit", "update", and "delete" buttons

4. Create a contact with a long name
    Step 1: Open the app and create a contact with a long name (more than 80 characters)
    Expected outcome: The entered name, phone number, and email are displayed in the first row

5. Create a contact with non latin characters
    Step 1: Open the app and create a contact with name using non latin characters (e.g. Chinese, Japanese, Korean)
    Expected outcome: The entered name, phone number, and email are displayed in the first row

6. Create a contact with symbols
    Step 1: Open the app and create one contact with name using some symbols (e.g. " ' ? # & *)
    Expected outcome: The entered name, phone number, and email are displayed in the first row

7. Create a contact without email
    Step 1: Open the app and create one contact without entering anything in the email field
    Expected outcome: Only the entered name and phone number are displayed in the first row

8. Create a contact without phone number
    Step 1: Open the app and create one contact without entering anything in the phone number field
    Expected outcome: Only the entered name and email are displayed in the first row

9. Create a contact without name
    Step 1: Open the app and create one contact without entering anything in the name field
    Expected outcome: Only the entered phone number and email are displayed in the first row

10. Create an empty contact
    Step 1: Open the app and click on the 'add' button without entering any contact information
    Expected outcome: Empty contact should NOT be created, i.e. no row should be added

11. Create multiple contacts
    Step 1: Open the app and create one contact
    Expected outcome: The entered name, phone number, and email are displayed in the first row
    Step 2: Create another contact
    Expected outcome: The newly entered name, phone number, and email are displayed in the second row
    Step 3: Create another contact
    Expected outcome: The newly entered name, phone number, and email are displayed in the third row

12. Create two contacts with identical information
    Step 1: Open the app and create one contact
    Step 2: Create one other contact with identical information from the first one
    Expected outcome: The entered name, phone number, and email are displayed both in the first and second row

13. Update contact email
    Step 1: Open the app and create one contact with name, phone number, and email
    Step 2: Click on the "edit" button
    Step 3: Replace the email information of the first contact with a new email
    Step 4: Click on the "update" button
    Expected outcome: The name, phone number, and the newly updated email are displayed in the first row

14. Update contact phone number
    Step 1: Open the app and create one contact with name, phone number, and email
    Step 2: Click on the "edit" button
    Step 3: Replace the phone number information of the first contact with a new phone number
    Step 4: Click on the "update" button
    Expected outcome: The name, email, and the newly updated phone number are displayed in the first row

15. Update contact name
    Step 1: Open the app and create one contact with name, phone number, and email
    Step 2: Click on the "edit" button
    Step 3: Replace the name information of the first contact with a new name
    Step 4: Click on the "update" button
    Expected outcome: The phone number, email, and the newly updated name are displayed in the first row

16. Update all contact information
    Step 1: Open the app and create one contact with name, phone number, and email
    Step 2: Click on the "edit" button
    Step 3: Replace the name, phone number, and email information of the first contact with new contact information
    Step 4: Click on the "update" button
    Expected outcome: The newly updated name, phone number, and email are displayed in the first row

17. Delete contact
    Step 1: Open the app and create 5 ordered contacts (#1, #2, #3, #4, and #5)
    Step 2: Click on the "delete" button of the first contact (#1)
    Expected outcome: Only 4 contacts remain and contact #2 is now displayed in the first row
    Step 3: Click on the "delete" button of the contact #4
    Expected outcome: Only 3 contacts remain and contact #5 is now displayed in the third row
    Step 4: Click on the "delete" button of the contact #5
    Expected outcome: Only 2 contacts remain and contact #3 is now displayed in the second row
    Step 4: Click on all the remaining "delete" buttons
    Expected outcome: All contacts are removed

***********************************
*********** Test Report ***********

This report is based on the manual testing performed on the above test cases

Test Case 1: SUCCESS
    Note: Expected title is displayed

Test Case 2: SUCCESS
    Note: First contact successfully created

Test Case 3: SUCCESS
    Note: 'add', 'edit', 'update', and 'delete' buttons are functioning properly

Test Case 4: SUCCESS
    Note: Contact with a long name successfully created

Test Case 5: SUCCESS
    Note: Contact with a name name using non latin characters successfully created

Test Case 6: SUCCESS
    Note: Contact with a name name using some symbols successfully created

Test Case 7: SUCCESS
    Note: Contact without email successfully created

Test Case 8: SUCCESS
    Note: Contact without phone number successfully created

Test Case 9: SUCCESS
    Note: Contact without name successfully created

Test Case 10: FAILED
    Note: Empty contact was created

Test Case 11: SUCCESS
    Note: Multiple contacts successfully created and stored in the right order

Test Case 12: SUCCESS
    Note: Contact with identical information successfully created

Test Case 13: SUCCESS
    Note: Email information successfully edited and updated

Test Case 14: SUCCESS
    Note: Phone number information successfully edited and updated

Test Case 15: FAILED
    Note: Updating name information causing phone number and email information being removed

Test Case 16: FAILED
    Note: Updating all information (name, phone number, and email) causing phone number and email information being removed

Test Case 17: SUCCESS
    Note: A contact in any given order from several contacts successfully deleted

***********************************
************ Bug Report ***********

As we have observed from the above manual testing, test case #10, #15, and #16 are failed
In conclusion, we have found 2 bugs:

Bug 1: Empty contact is created when user click 'add' button before entering any contact information

Bug 2: Updating name information causing phone number and email information being removed

***********************************
****** Request of Enhancement *****

Enhancement proposal 1: Apply validation before saving contact information

Enhancement proposal 2: While editing an existing contact information, disable 'add' button

Enhancement proposal 3: While editing one contact, disable 'edit' and 'delete' button on other contacts
